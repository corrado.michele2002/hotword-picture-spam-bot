## How to use it
Rename the `dummyDockerfile` file to `Dockerfile` and fill the ENV variables with your API ID and bot token.
Put all the pictures you want to be randomly sent in the `media` folder inside the `app` folder.
Then run `docker compose up`.
